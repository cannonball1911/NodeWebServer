const colors = require('colors')

let lLog = (message) => {
    console.log('[SERVER]: '.yellow + message)
}

module.exports.lLog = lLog