const express = require('express')
const hbs = require('hbs')
const fs = require('fs')

const myFuncs = require('./myFuncs.js')

const port = process.env.PORT || 3000
let app = express()

hbs.registerPartials(__dirname + '/views/partials')
app.set('view engine', 'hbs')

app.use((req, res, next) => {
    let now = new Date().toString()
    let log = `${now}: ${req.method} ${req.url}`
    myFuncs.lLog(log)
    fs.appendFile('server.log', log + '\n', (err) => {
        if (err) {
            myFuncs.lLog('Unable to append to server.log')
        }
    })
    next()
})

// app.use((req, res, next) => {
//     res.render('maintenance.hbs')
// })

hbs.registerHelper('getCurrentYear', () => {
    return new Date().getFullYear()
})

hbs.registerHelper('screamIt', (text) => {
    return text.toUpperCase()
})

app.use(express.static(__dirname + '/public'))

app.get('/', (req, res) => {
    res.render('home.hbs', {
        pageTitle: 'Home Page',
        welcomeMessage: 'Hello guys, welcome to the Homepage'
    })
})

app.get('/about', (req, res) => {
    res.render('about.hbs', {
        pageTitle: 'About Page'
    })
})

app.get('/projects', (req, res) => {
    res.render('projects.hbs', {
        pageTitle: 'Projects Page'
    })
})

app.get('/bad', (req, res) => {
    res.send({
        errorMessage: 'FAILED'
    })
})

app.listen(port, () => {
    myFuncs.lLog(`Server is up on port ${port}`)
})